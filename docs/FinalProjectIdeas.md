# FINAL Project IDEAs:

1. Squeeazy: Clothes squeezing/wringing machine
   ![Idea1](./1.jpg)

2. Blackboard wiper
   ![Idea2](./2.jpg)

3. Rice Sieve/sift system
   ![Idea3](./3.jpg)

4. Automated retractable spoiler for an automobile
    ![Idea4](./4.jpg)

