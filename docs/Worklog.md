# Documentation Time Line:

## 29/01/22 

### 10:30 am

1. Watched User Innovation - [User Innovation](https://www.youtube.com/watch?v=Cbydupv1EjQ&ab_channel=MITBootcamps)
2. Read Adapt Code of Practice -[AdaptManifesto](https://www.adaptmanifesto.org/adapt-code-of-practice-principles)
3. Began Brainstorming ideas for final project
4. Came up with 4 possible ideas which can be found on my FinalProjectIdeas.md file

### 3:30 pm

1. Began Downloading Visual studios for my windows computer
2. Began Downloading Homebrew for my MacOS
3. Ran into many installation and downloading issues so worked with Fran and Sibu who helped me trouble shoot and finally solved the issue

## 30/01/22

### 8:30 pm

1. Watched the Markdown tutorial [MarkdownCrashcourse](https://www.youtube.com/watch?v=HUBNt18RFbo)
2. Downloaded suggested extentions
