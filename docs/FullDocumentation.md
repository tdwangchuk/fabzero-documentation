# Main link for Fabzero:
[fabZero](https://gitlab.fabcloud.org/fabzero/fabzero/-/blob/master/program/summary.md) 

---
---
---
**Documentation Begins:***

># MARKDOWN

### Headers
Headers are defined by the '#'symbol. One '#' for H1, two for H2, etc.

### Quotes
Quotes are defined by the '>' symbol

### Bold/Italics
Add emphasis with asterisks '*' and underscores '_' Two before and after (no spaces) a section of texts makes it bold
One before and after (no spaces) a section of texts makes it italic
You can also put Bold and Italicized text inline by surrounding a group of words.

### Horizontal Break
A horizontal rule gives a visible line break. You can create one by putting three or more hypens, asterisks, or underscores (-, *, _).

### lists
Create unordered lists using '-', '*', '+,
You can create sublists by indenting
Create ordered lists using a number prefix

### link
Create a link with text by surrounding text with brackets, [], and link immediately following with parenthesis ()

### image
Defining an image is similar to defining a link, except you prefix it with '!'
Just like links, you can define images by reference in the same format.

### code
You can do inline code with backticks (``)
You can do blocks of code by surroung it with 3 backticks 

### Tables
Tables are useful for displaying rows and columns of data. Column headers can be defined in between pipes (|)

---
---



># GIT/Terminal

* always ``git status``

* followed by ``git pull``

* for initial cloning using https `` git clone (paste the https here) ``

* to control directory ``cd``
* To select directory eg. `` cd desktop``
* To check contents in said directory ``ls``
* To clear the terminal page ``clear``
* To move new vs files into the gitlab local folder: ``mv file.md foldername``
(ensure in same directory)

* after moving the file into the folder `` git add .`` or ``git add filename.md``
* then commit `` git commit -m "with a meaning full message'' ``
* Finally push ``git push``
  
  
**To edit files that are already pushed**

1. Open the entire folder in VS
2. edit said file
3. will get alert that changes are pending
4. hit commit
5. the gitlab will automatically be updated



># CLONING USING SSH 
 1. Generate SSH key

   `` $ ssh-keygen -t ed25519 -C "your_email@example.com" ``


 2. Add key to ssh-agent

    `` $ eval "$(ssh-agent -s)" ``

3. Add private key to ssh-agent

    `` $ ssh-add -K ~/.ssh/id_ed25519``

4. Copy Public SSH and paste in SSH in gitlab in preferences
5. clone the repo in terminal

    ``git clone`` *Paste the URL from gitlab*
6. `` git clone `` & ``git pull``

**USEFUL LINK:** [SSH key generation](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent)

---
---

># PROJECT MANAGEMENT

* The project managers of respective labs will create public and private project
* Issue: 
  * Title:make short and descriptive
  * Description: should be comprehensive
* Milestone: definitive goals
* Assign: assign certain tasks to individuals
* Labels: Generate default labels
* *Board: create a todo and doing list
  * can drag issues between lists
  
  Useful link: [ProjectManagement](https://gitlab.fabcloud.org/fabzero/fabzero/-/blob/master/program/basic/projectmanagement.md)

  
  ---
  ---

># CAD 2D: Raster and Vector
  
* Machines in the fablab accept Raster and Vector
* Raster: in Pixels
* Vector: Mathmatical
* We should be able to go from raster to vector and vice versa
* Machines that follow a path use Vector 
* Other machines use Raster. (Rasters greyscale can be used as a 3D model)(Raster in laser cutting will engrave rather then cut)
  
  ---
  ---

  
># Creating a Temp Folder to keep uncompressed images

1. in terminal: nano gitignore
2. write temp/ (or the folder you want to hide)
3. Save
4. add, commit and push
5. Then add all uncompressed pics into the the temp folder

### Useful functions:
``ll -h`` returns details of the image in readable form
``la`` shows hidden files
``ls -a`` Shows all unhidden files

---

># Flameshot and IMAGEMAGICK


Utilize Flameshot to screenshot and edit pics, and then move the screenshot to the temp folder for compressing. and follow the following:

* To install: ``sudo apt install imagemagick in Linux/WSL`` or ``brew install imagemagick in macOS``
* To Resize (but not replace): ``convert bigimage.jpg -resize 640x640 smallimage.jpg``
* To convert and replace: ``mogrify -resize 640x640 *.jpg``
* To combine images in a horizontal line: ``convert image1.jpg image2.jpg -geometry x400 +append stripimage.jpg``
* To convert between formats: ``convert example.png example.jpg``
* To duplicate: ``cp image.png cp image2.png``

Once compression and all edits are done, move the images to the images folder or the main folder

Helpful link: [2D and Image](https://gitlab.fabcloud.org/fabzero/fabzero/-/blob/master/program/design/cad2d.md)

---
---
># INKSCAPE

### Raster to Vector
1. Upload raster png
2. click on the drawing and click ``Trace Bitmap``
   1. Single Scan for Black&White
   2. Multiple Scan for Coloured
3. Adjust Brightness cutoff: higher number makes the line thicker
4. Click apply once you are done editing and tracing
5. Hit the lock icon to keep aspect ratio
6. W = 210 mm for A4 size
7. Click ``document properties`` to check sizing
8. Resize page to drawing
9. After editing parameters for the white space
10. Hit Resize page to drawing again
11. And save file in proper folder 
  
### Vector to Raster
1. File-> export png image -> page
   1. For printing dpi = 200-300
   2. For machines dpi = 600
   3. For super precise (pcbs etc) = 1200
  * 5cm wide ideal size for documentation
2. Choose proper folder and save

># Video Tools

## youtube-dl
* brew install youtube-dl
   
   Once done:

1. cd desktop/fabzero-documentation/temp/
3. then in terminal: `` youtube-dl *Paste Url Here* ``
4. This will download the video with best audio and resolution.

**If you want tto select res/audio:
*To check for all the formats: ``youtube-dl -F *paste url here*``

Pick the code (left hand side)

* then: ``youtube -f007 *vid url*``

## Losslesscut
can be used to trim parts of videos. (if you only want certain sections)

Steps:
1. Drag the video into the app
2. Spacebar to play the vid
3. press i : beginning of first section
4. press o : end of first section
5. press i : beginning of next section
6. press o: end of this secion
7. and so on as many sections as needed
8. Hit Export
9. select formating, path and name

USEFUL LINK: [Video Tools](https://gitlab.fabcloud.org/fabzero/fabzero/-/blob/master/program/design/video.md)

># INSCAPE AND LOSSLESSCUT ASSIGNMENT

Raster to Vector: ![Audi SVG](../Images/Audi.svg)

Vector Back to Raster: ![Audi High Res Png](../Images/Audihr.png)

Trimmed Clip: [Siu](/videos/siu-00.00.01.427-00.00.04.926.mp4)

---
---


># KiCAD

Demo PCb: ![Demo PCB](../Images/demoPCB.PNG)

Assignment 1: ![SimplePCB](../Images/SimpleCircuit.PNG)

Demo PCB SCH file : [demo sch](/pcb/Assignment1.kicad_sch)


## KiCAD Workflow and Tips

### Short cuts:
  ``m`` = move component

  ``r`` = rotate component

  ``a`` = add component
  
  `` ctrl+c, ctrl v`` = copy/paste

  ``g`` = grab component and connection

  ``e`` = assign value (after hovering over component)

### Basic Steps:

1. Add components and connect them
1. annotate schematic symbols (top)
1. assign values (hover and press 2)
1. generate PCB, click generate netlist (top)
1. click generate netlist again
1. Assign PCB footprints to schematic symbols (on top)
1. Click component in the middle tab, select a name on the left, and then exact component on the right
1. click "view selected tool print" (top left), to see if it suits your needs
1. After assigning: apply, save, continue (bottom right)
1. Generate netlist again
1. Run PCB new to layout printed circuit board
1. Click "Neti Load" [net] (top)
1. select file, update pcb, close
1. rearrange so no wires overlap
1. top left: change/select track/sizes
1. top right select which layer: F.Cu (pgUp) and select B.cu (PgDn)
1. Select "Route Tracks" on the right and start routing your circuit (follow wire pathing)
1. Incase of overlap, change the layer from PgUp to PgDn
1. If you want to proceed without manually changing the layer, take the route to where it would overlap, then right click and select "place throuh via", double the route. will automatically switch to another layer
1. If `` unrouted = 0 `` in the bottom, NO ERROR
1. top right (F.Cu), select edge cuts
1. select "add graphic  lines" from the right
1. select "change cursor type" from the left
1.  start drawing the edge
1. view -> 3D viewer
1. To add mounting holes:

   * "Add footprints" on the right
   * select empty path/space and click on it
   * select mounting holes (usual 3.2 mm, M3)
1. To change names for 3D viewer, go back to the pcb and hover over the component, press ``e`` and then change the mounting hole name to M3
1. Make sure name/everything is inside the edge cuts (Blue colored names)
1. Under layers, ignore F.fab and B.fab unless used, but select everythig else
1. To add custom naming: 
  * Add `text` on the right **[T]**
  * select F.Silks
  * Select an empty space and write whatever

31. To add fill zones: 
  * select copper layer (Pg.Up)
  * add fill zones (on the right)
  * select edge cut
  * select any pad
  * hit ok
  * then select all the edgecuts

32. Do the same for other side/layer (B.Cu(pgdn))

33. For gerber files:
  * click on plot (top left)
  * make a folded called gerber
  * make sure gerber format is selected
  * make sure FCU, Bcu,Fsilks, Bsilks, edgecuts, Fmask,Bmask (and anything used) is selected
  * can ignore rest/unused
  * click plot
  * generate drill files
  * generate drill files
  * zip all files

  [Usefull Link](https://www.youtube.com/watch?v=-tN14xlWWmA&ab_channel=MakeStuff)


### **KiCAD day 2 assignments:**

demo pcb: ![demo pcb](/Images/demopcbfile.png)

demo pcb schematic file: [pcb schematic](/pcb/DemoTrial.kicad_sch)

demo pcb file : [pcb](/pcb/DemoTrial.kicad_pcb)

### **KiCAD day 3 assignments:***

Gerber Zip: [gerber files](/pcb/demogerber.zip)

Svg files: ![SVG](/Images/DemoTrial-F_Cu.svg)

PNG files: ![PNG upper](/Images/Fcu.png)
&
![PNG EDGE](/Images/outer.png)

**Size:18.3 x 11.1 mm**

#### Tips for today:
  1. When tracing:
    
    * u = select entire trace
    * w = increase width of trace
    * shift w = decrease width of the trace

  2. Export for manufacturing
    * file, fabrication options, gerber?

  3. Export as svg:
    * select edge on all layers
    * negate image
  4. Open in inkcaspe: 

    * select only the white part and export as png. dpi=1500
   *  Do the same for the edge 
   * Invert edge in Gimp


### **KiCAD Day 3 Assignments:

Gerber Files: [Gerber Zip](/pcb/demotrialtwo.zip)

PNG IMAGES: ![TOP](/Images/DTTC_topNew.png)

&&

![Drill](/Images/DTTCDrill.png)

&&

![Bottom Cut](/Images/DTTC_BotNewCut.png)

&&

![Bottom](/Images/DTTC_BotNew1.png)

---
---

># Stickers

Halftoning:
  * import the jpg
  * adjust brightness/contrast
  * distort -> newsprint
  * utilize line or circle and adjust angle and period accordingly.

  Half Toning Assignment: ![SennaBW](/Images/sennasticker.png)

  Colored Sticker: ![tiger](/Images/tiger.svg)
  
  ---
  ---
  
  > ## Neil's way of PCB design

  quick tips:
  1. ``head file name`` = shows first 10 lines of the file
  2. ``cat filename`` = shows content of the file

  * feh file.png -R 2 &
  (opens the file, refreshes every 2 seconds, allows to use termina)

  * python3 pcb.py | python3 frep-C.py 500
   
   ---
   ---
   ---

## Fabzero Machine Trainings

### Vinyl Cutter
  
  1. Power on the machine
  1. Release the lock on the top-left of the machine 
  1. Insert Vinyl
  1. Ensure that the vinyl holder rollers are both within any two white Stickers
  1. Clamp on the lock on the top-left
  1. On the control panel, select the type (roll,sheet,edge) and hit entire
  1. Send cutting instructions from the computer to the machine
  1. If using a roll, the origin can be set by holding the "set origin" button
  
  Important:
  1. handle the blade tip with utmost create
  1. to remove the blade holder, losen the orange screw and remove the black blade holder.

### ShopBot

**SAFETY** 
1. Wear boots
1. Avoid long sleeves (fold up sleeves if wearing long sleeves)
1. Wear Eye protection
1. Wear and reuse N95 masks
1. Wear ear protection if required
1. Milling bits are very sharp and can cut unknowing. Handle with care 
1. Be careful of the greese on the edges of the machine (kiss of the shopbot)
1. Press any of the two buttons in case of emergency
1. The operator can never leave the room
1. After pressing the red button, rotate to release.

**OPERATION**
1. Turn on the power supply and then the control box
1. On operator handle, switch on the spindle
1. Put key in for operating the spindle
1. Start shopbot3 on the computer (it is the control center)
1. 5 Error numbers and their designations:  
    `1  = z axis zero plate`
    `2  = X axis`
    `3  = Y axis`
    `4  = Operator control (hit reset)`
    `5  = z axis`

1. Remove the nut, put spindle in a right collat and then insert into the nut and return it back to its original position.
1. Set zeros:
1. Start with xy zeroing
1. Then zero z 
1. To zero z axis, hit the z axis button on the control center and then remove the plate, set it under the spindle and go ahead (follow the instructions on the control panel)
1. To set user zeros, move the spindle to the wanted position using the joy stick and then hit zero axes and note down the values incase of a outage

* In the case of a power outage:
1. restart control box
1. Start controller
1.  rezero the machine and rezero to user zeros
1. Begin functioning again

### Trotec Speedy 400 flex Laser Cutter

**Safety**
1. Only cut if you know what the material is
1. Check if internal air blower is working (*shift + fan sign)
1. After using the machine, do not open the lid for atleast one minute
1. Can only cut or engrave thermoplastics
1. Fiber can engrave metals
1. CO2 cant work with metals
1. CO2 can engrave glass
1. with moleybden grease, you can engrave on stainless steel using CO2 
1. Lens and mirror can be damaged by fire so handle with care
1. keep eyes on the machine at all times

**Fire Hazard**

Incase of Fire:

1. Hit emergency button
1.  do not open the lid and observe the Flame
1. if it continues, manually push back the lens axis
1. If the fire is small, remove the material and stamp on it or use water if necessary
1. if its a big fire, remove fire blanket and cover the Flame
1. if its too big to handle, ignore machine and extinguish flames to save the building

**Maintainence**

1. Lift the grill by releaseing the pins on each side and vacuum
1. check the tightness of the bolts on the side of lens/laser head
1. opnen the red side to inspect the mirror, if dirty, spots or hazy, wear black gloves, use a zeiss wet wipe and wipe the lens. wipe and wait for the alcohol to evaporate.
1. use cleaning fluids to clean the inner sides of the laser Cutter

** Functioning**
1. Turn on power, turn on key, it will beep, auto home and then beep again.
1. After second beep, can open the hood
1. Shift + fan to test if the internal blower is working. place finger under the lens to feel for air flow 
1. put the focus tool on the head ridge,
1. once on, lift the z axis until the focus bit just about falls. this means it is focused
1. Focus on the center of the job
1. Always refocus when changing work, power outage or changing material




